# Docker Workshop
Lab 01: Basic commands

---

## Overview

In this lab you will learn the basic commands of docker that allow you manage images and containers in your docker host.


## Notes

 - During the lab we will use the following image (this image contain a basic web application):
 
   https://hub.docker.com/r/selaworkshops/npm-static-app/


## Instructions

 - Create a new container from the image without running it:
```
$ docker create --name app-stopped selaworkshops/npm-static-app:latest
```

 - Create and run a new container (in detached mode) using:
```
$ docker run -d -p 80:3000 --name app-running selaworkshops/npm-static-app:latest
```

 - Check that the second container is up and running:
```
$ docker ps
```

 - Check that the first container was created but is not running:
```
$ docker ps -a
```

 - Browse to the running container application:
```
http://<your-server-ip>:80
```

 - Check which images exist in your host:
```
$ docker images
```

 - Let's create another tag for the image:
```
$ docker tag selaworkshops/npm-static-app:latest mytag:latest
```

 - Check which images exist in your host (note that both images have the same Id)
```
$ docker images
```

 - Remove both containers (with the force flag):
```
$ docker rm -f app-running app-stopped
```

 - Remove the containers images:
```
$ docker rmi selaworkshops/npm-static-app:latest
```
```
$ docker rmi mytag:latest
```

 - Check the containers status:
```
$ docker ps -a
```

 - Check the images status:
```
$ docker images
```
